import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'delegate-demo',
  templateUrl: './delegate.component.html',
  styleUrls: ['./delegate.component.scss']
})
export class DelegateComponent implements OnInit {

  @Input() delegate: DemoDelegate;

  constructor() { }

  ngOnInit() {
    console.log(this.delegate);
    this.delegate.done = this.done;
  }

  done() {
    console.log('delegate done');
  }

}

export interface DemoDelegate {
  done?: Function;
  show: Function;
}
